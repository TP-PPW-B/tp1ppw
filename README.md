# TP1ppw

# Kelompok 6 PPW B Tugas 1
Members: 
- Dinda Mutiara Qur’ani Putri - 1706984556 
- Monalisa Valencia Merauje - 1706103543
- Dimas Krissanto Rahmadi - 1706979202
- Rindu Salsabilla Chandra - 1706979442
    
Status Pipelines:
[![pipeline status](https://gitlab.com/TP-PPW-B/tp1ppw/badges/master/pipeline.svg)](https://gitlab.com/TP-PPW-B/tp1ppw/commits/master)

[![coverage report](https://gitlab.com/TP-PPW-B/tp1ppw/badges/master/coverage.svg)](https://gitlab.com/TP-PPW-B/tp1ppw/commits/master)



Link Heroku:
https://ppw-b-kelompok-6.herokuapp.com/