from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('donasi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mymodels',
            name='check',
            field=models.BooleanField(default=True),
        ),
    ]
