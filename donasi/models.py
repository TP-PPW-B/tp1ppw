from django.db import models
from django.urls import reverse

# Create your models here.
class MyModels(models.Model):
    program = models.CharField(max_length=30)
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=100)
    uang = models.CharField(max_length =30)
    check = models.BooleanField(default=False)

    # def get_absolute_url(self):
    #     return reverse('index', kwargs={'id':self.id})
