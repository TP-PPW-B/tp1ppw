from django.shortcuts import render
from .models import MyModels
from .forms import FormPage
from Beranda.models import Program
from daftar.models import Pengguna
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def donasi_view(request, id):
    data = Program.objects.get(id=id)
    username = None
    if request.user.is_authenticated:
        username = request.user.username
    response = {'form': FormPage(),
                'data': data,
                'username': username}
    return render(request, "Donasi.html", response)

def donatur_view(request):
    result = MyModels.objects.all()
    response = {'result': result}
    return render(request, "Donatur.html", response)

def formView(request):
    response = {}
    donasi = FormPage(request.POST or None)
    if (request.method == 'POST' and donasi.is_valid()):
        response['program'] = request.POST['program']
        response['nama'] = request.user.username
        response['email'] = request.user.email
        response['uang'] = request.POST['uang']
        check = True if 'check' in request.POST else False
        donasi_form = MyModels(program = response['program'],
                               nama = response['nama'],
                               email = response['email'],
                               uang = response['uang'],
                               check = check)
        donasi_form.save()
        return HttpResponseRedirect('/thankyou/')
    else:
        return HttpResponseRedirect('/program/')

def thankyou(request):
    return render(request, "thankyou.html")
