from django.test import TestCase
from django.test import Client

from django.urls import resolve
from .views import *
from .models import MyModels
from .views import donasi_view
from .views import donatur_view


from django.urls import resolve
from .views import *
from .models import MyModels

from .views import donasi_view
from .views import donatur_view

class TP1UnitTest(TestCase):

    def test_donasi_url(self):
        program = Program()
        program.save()
        response = self.client.get("/donasi/1/")
        self.assertTemplateUsed(response, "Donasi.html")

    def test_donatur_url_is_exist(self):
        c = Client()
        response = c.get('/donatur/')
        self.assertEqual(response.status_code,200)

    def test_program_url_is_exist(self):
        c = Client()
        response = c.get('/program/')
        self.assertEqual(response.status_code,200)

    def test_donatur_index_func(self):
        found = resolve('/donatur/')
        self.assertEqual(found.func, donatur_view)

    def test_template_donatur(self):
        response = Client().get('/donatur/')
        self.assertTemplateUsed(response, 'Donatur.html')

    def test_template_thankyou(self):
        response = Client().get('/thankyou/')
        self.assertTemplateUsed(response, 'thankyou.html')

    def test_submit_model(self):
        status_message = MyModels.objects.create(program = 'test',
        nama = 'rindu',
        email = 'r@g.com',
        uang = '1000',
        check = False)
        counting_object_status = MyModels.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_submit_form(self):
        c = Client()
        response = c.post('/postdonasi', {'uang' : 'Rp230191'})
        self.assertEqual(response.status_code, 301)
