from django.urls import path
from . import views

urlpatterns = [
    path('',views.home, name = 'home'),
    path('program/',views.program, name = 'program'),
    path('history/',views.history, name = 'history'),

]