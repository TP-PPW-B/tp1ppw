from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Tp1UnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_program_url_is_exist(self):
        response = Client().get('/program/')
        self.assertEqual(response.status_code,200)
    def test_beranda_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func,home)

    def test_program_index_func(self):
        found = resolve('/program/')
        self.assertEqual(found.func, program)

    def test_template_beranda(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_template_program(self):
        response = Client().get('/program/')
        self.assertTemplateUsed(response, 'program.html')
    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Donasi Anda',html_response)
        self.assertIn('adalah inspirasi orang lain',html_response)

    def test_login_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('LOGIN',html_response)

    def test_program_page_(self):
        response = Client().get('/program/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Siap memberi bantuan?',html_response)
        self.assertIn('Hallo! Pilih program yang ingin anda bantu :)',html_response)

    def test_history_url_is_exist(self):
        response = Client().get('/history/')
        self.assertEqual(response.status_code,200)
    
    def test_history_func(self):
        found = resolve('/history/')
        self.assertEqual(found.func, history)

    def test_template_history(self):
        response = Client().get('/history/')
        self.assertTemplateUsed(response, 'home.html')
    
    
