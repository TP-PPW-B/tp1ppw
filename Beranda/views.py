from django.shortcuts import render
from donasi.models import MyModels
from .models import Program

# Create your views here.
def home(request):
    return render(request, 'home.html')
def program(request):
    programs = Program.objects.all()
    return render(request, 'program.html',{'programs':programs})
def history(request):
    if request.user.is_authenticated:
        result = MyModels.objects.all().filter(nama = request.user.username)
        response = {'data': result}
        return render(request, 'history_page.html',response)
    else:
        return render(request, 'home.html')