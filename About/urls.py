from django.urls import path
from django.contrib.auth import views
from django.conf import settings
from django.conf.urls import url, include
from .views import about,create_post


urlpatterns = [
    path('about-us', about, name='about-us'),
    path('post',create_post,name = 'post')
]