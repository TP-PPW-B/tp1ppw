from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import about
from .models import Testimonial
from .forms import FormTestimoni


# Create your tests here.
class About(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('/about-us')
        self.assertEqual(response.status_code, 200)
    
    def test_about_using_func(self):
        found = resolve('/about-us')
        self.assertEqual(found.func, about)
    
    def test_using_about_template(self):
        response = Client().get('/about-us')
        self.assertTemplateUsed(response, 'about.html')
    
    def test_models_can_create_testimonial(self):
        testimoni = Testimonial.objects.create(testimoni = 'contoh testi',)
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial, 1)

    def test_post_url_is_exist(self):
        response = Client().get('/post')
        self.assertEqual(response.status_code, 200)
    
    def test_can_save_a_POST(self):
        response = self.client.post('/about-us', data = {'testimoni':'save testi',})
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], 'about-us/')

        new_response = self.client.get('/about-us')
        html_response = new_response.content.decode('utf8')
        self.assertIn('', html_response)