from django.db import models
from datetime import datetime,date
from django.utils import timezone

# Create your models here.
class Pengguna(models.Model):
    nama = models.CharField(max_length=30)
    datebirth = models.DateField(default=timezone.now)
    email = models.EmailField(max_length=100, unique = True)
    password = models.CharField(max_length=50, default ="")

class Berita(models.Model):
    img_berita = models.CharField(max_length = 300)
    judul_berita = models.CharField(max_length = 50)
    isi_berita= models.CharField(max_length = 100)
    

