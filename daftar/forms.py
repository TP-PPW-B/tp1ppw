from django.db import models
from datetime import datetime, date
from django import forms
from .models import Pengguna

class FormPage(forms.Form):
    attrs = {
        'class':'form-control'
    }
    nama = forms.CharField(label='Nama Lengkap', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    datebirth = forms.DateField(label = 'Tangal Lahir',required=True,widget=forms.DateInput(attrs={'type':'date'}))
    email = forms.EmailField(label='Email', max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label ='Password',max_length = 50, required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
