from django.shortcuts import render
from .models import Pengguna
from .models import Berita
from .forms import FormPage
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
# Create your views here.

def donasi_view(request):
    print("Donasiview")
    response = {'form': FormPage}
    return render(request, "Daftar.html", response)

def formView(request):
    print("Test")
    response = {}
    donasi = FormPage(request.POST or None)
    if (request.method == 'POST' and donasi.is_valid()):
        print("valid")
        response['nama'] = request.POST['nama']
        response['datebirth'] = request.POST['datebirth']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        donasi_form = Pengguna(nama = response['nama'],    
                                datebirth= response['datebirth'],
                                 email = response['email'],
                                 password = response['password'])
        donasi_form.save()
        return HttpResponseRedirect('/program/')
    else:
        return HttpResponseRedirect('/daftar/')

def berita_view(request):
    beritas = Berita.objects.all()
    return render(request, 'news.html',{'beritas':beritas})

def logout_view(request):
    request.session.flush()
    logout(request)
    return  HttpResponseRedirect('/')