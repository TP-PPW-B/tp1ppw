from django.test import TestCase
from django.test import Client

from django.urls import resolve
from .views import *
from .models import Pengguna
from .views import *


class TP1UnitTest(TestCase):

    def test_daftari_url_is_exist(self):
        c = Client()
        response = c.get('/daftar/')
        self.assertEqual(response.status_code,200)

    def test_berita_url_is_exist(self):
        c = Client()
        response = c.get('/berita/')
        self.assertEqual(response.status_code,200)

    def test_daftar_index_func(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, donasi_view)

    def test_berita_index_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, berita_view)

    def test_template_daftar(self):
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, 'Daftar.html')

    def test_template_berita(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'news.html')

    def test_submit_form(self):
        c = Client()
        response = c.post('/daftar/', {'program' : 'GW jago PPW'})
        self.assertEqual(response.status_code, 200)

    def test_submit_form2(self):
        c = Client()
        response = c.post('//', {'nama' : 'mona'})
        self.assertEqual(response.status_code, 200)


    
        

    def test_invalid_input(self):
            daftar_form = FormPage({
                'program': 31 * "X"
            })
            self.assertFalse(daftar_form.is_valid())


